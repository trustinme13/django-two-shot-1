from django.contrib import admin

from receipts.models import Account, ExpenseCategory, Receipt

# Register your models here.


class AdminForm(admin.ModelAdmin):
    admin.site.register(Account)
    admin.site.register(Receipt)
    admin.site.register(ExpenseCategory)
